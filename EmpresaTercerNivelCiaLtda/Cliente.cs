﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmpresaTercerNivelCiaLtda
{
    class Cliente
    {
        private string nombre;
        private string apellido;
        private string cedula;
        private string direccion;

        public string Nombre
        {
            get => nombre; 
            set => nombre = value; 
        }

        public string Apellido
        {
            get => apellido; 
            set => apellido = value; 
        }

        public string Cedula
        {
            get => cedula;
            set => cedula = value;
        }

        public string Direccion
        {
            get => direccion;
            set => direccion = value;
        }
    }
}
