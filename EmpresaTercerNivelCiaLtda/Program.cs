﻿using System;

namespace EmpresaTercerNivelCiaLtda
{
    class Program
    {
        /*En la empresa Tercer Nivel Cía. Ltda. existe un requerimiento para realizar una aplicación que permita calcular el precio a cobrar por ventas de Galones de Gasolina, para la realización de la aplicación tenga en cuenta los siguientes requisitos funcionales:
        La aplicación debe solicitar los datos del Cliente, que son Apellidos, Nombres, Número de Cédula de identidad y Dirección.
        La aplicación debe solicitar los datos de la Gasolina, que son Tipo, Cantidad de Galones y Precio de Venta.
        Si el tipo de Gasolina es “Extra” entonces el precio de la gasolina será de $1.50 por galón, caso contrario la Gasolina será “Super” por lo que el precio será de $2.00 por galón.
        Al cálculo debe aumentar el impuesto al valor agregado I.V.A.del 12%.
        La salida por pantalla debe mostrar los datos del Cliente, los datos de la Gasolina y el valor final a cancelar (Subtotal, IVA y Total).
        */

        static void Main(string[] args)
        {
            Cliente cliente = new Cliente();
            Console.WriteLine("Ingrese su nombre");
            cliente.Nombre = Console.ReadLine();
            Console.WriteLine("Ingrese su apellido");
            cliente.Apellido = Console.ReadLine();
            Console.WriteLine("Ingrese su cedula");
            cliente.Cedula = Console.ReadLine();
            Console.WriteLine("Ingrese su direccion");
            cliente.Direccion = Console.ReadLine();

            Console.WriteLine("Nombre del cliente =" + cliente.Nombre + "\n" + "Apellido del cliente =" + cliente.Apellido + "\n" + 
                "Cedula del cliente =" + cliente.Cedula + "\n" + "Direccion del cliente=" + cliente.Direccion);
            Console.WriteLine();
            Ejecucion ejecucion = new Ejecucion();
            ejecucion.resultado();
        }
    }
}
