﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmpresaTercerNivelCiaLtda
{
    class Ejecucion
    {
        public void resultado()
        {
            int opcion;

            TipoGasolina tipo = new TipoGasolina();
            Console.WriteLine("Escoja el tipo de gasolina \n"
                        + "1. EXTRA \n"
                        + "2. SUPER \n");
            opcion = Convert.ToInt32(Console.ReadLine());
            switch (opcion)
            {
                case 1:
                    Console.WriteLine("Usted a escogido gasolina EXTRA");
                    Double SubTotal, Total, Impuesto, Iva = 0.12;

                    Console.WriteLine("Escoja los galones de gasolina");
                    tipo.Cant = Convert.ToInt32(Console.ReadLine());
                    SubTotal = tipo.Cant * tipo.Precio;
                    Impuesto = SubTotal * Iva;
                    Total = SubTotal + Impuesto;

                    Console.WriteLine("El SubTotal es= " + SubTotal + "\n" +
                        "El impuesto es=" + Impuesto + "\n" + "El Total a cancelar es" + Total);
                    Console.ReadLine();
                    break;

                case 2:
                    Console.WriteLine("Usted a escogido SUPER");
                    Console.WriteLine();

                    Double Impuesto1, Total1, SubTotal1, Iva1 = 0.12;
                    Console.WriteLine("Escoja los galones de gasolina");
                    tipo.Cant = Convert.ToInt32(Console.ReadLine());
                    SubTotal1 = tipo.Cant * tipo.Precio1;
                    Impuesto1 = SubTotal1 * Iva1;
                    Total1 = SubTotal1 + Impuesto1;

                    Console.WriteLine("El SubTotal es= " + SubTotal1 + "\n" +
                        "El impuesto es=" + Impuesto1 + "\n" + "El Total a cancelar es" + Total1);
                    break;
            }
        }
    }
}
